﻿using AssemblyAnaylzeModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AssemblyAnaylzeModule.Services
{
    public class ElasticAgent
    {
        private Globals globals;

        private List<clsOntologyItem> paramTypes = new List<clsOntologyItem>();

        public async Task<ResultItem<List<clsObjectRel>>> CheckClasses(List<ClassItem> classItems)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
           {
               var result = new ResultItem<List<clsObjectRel>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsObjectRel>()
               };

               var paramTypeNames = classItems.SelectMany(cls => cls.Members).SelectMany(memb => memb.Parameters).GroupBy(param => param.TypeName).Select(typeN => typeN.Key).ToList();

               var paramTypesToCreate = (from paramType in paramTypeNames
                                         join dataType in paramTypes on paramType equals dataType.Name into dataTypes
                                         from dataType in dataTypes.DefaultIfEmpty()
                                         where dataType == null
                                         select new clsOntologyItem
                                         {
                                             GUID = globals.NewGUID,
                                             Name = paramType,
                                             GUID_Parent = Config.LocalData.Class_DataType__Dot_Net_.GUID,
                                             Type = globals.Type_Object
                                         }).ToList();

               if (paramTypesToCreate.Any())
               {
                   var dbWriterDataTypes = new OntologyModDBConnector(globals);
                   result.ResultState = dbWriterDataTypes.SaveObjects(paramTypesToCreate);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving the Datatypes!";
                       return result;
                   }
                   paramTypes.AddRange(paramTypesToCreate);
               }

               var dbConnectorClassNamespaceRelation = new OntologyModDBConnector(globals);

               try
               {
                   var searchClassesToNamespaces = classItems.Select(cls => new clsObjectRel
                   {
                       ID_Other = cls.NamespaceItem.GUID,
                       ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID,
                       ID_Parent_Object = Config.LocalData.Class_Classes.GUID
                   }).ToList();

                   result.ResultState = dbConnectorClassNamespaceRelation.GetDataObjectRel(searchClassesToNamespaces);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the relations between classes and namespaces!";
                       return result;
                   }
               }
               catch (Exception)
               {

                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Error while getting the Relations between Classes and Namespaces!";
                   return result;
               }

               (from classItm in classItems
                join rel in dbConnectorClassNamespaceRelation.ObjectRels on new { IdNameSpace = classItm.NamespaceItem.GUID, NameClass = classItm.NameClass } equals new { IdNameSpace = rel.ID_Other, NameClass = rel.Name_Object } into rels
                from rel in rels.DefaultIfEmpty()
                select new { classItm, rel }).ToList().ForEach(relClass =>
                {
                    if (relClass.rel == null)
                    {
                        relClass.classItm.ClassOItem = new clsOntologyItem
                        {
                            GUID = globals.NewGUID,
                            Name = relClass.classItm.NameClass,
                            GUID_Parent = Config.LocalData.Class_Classes.GUID,
                            Type = globals.Type_Object,
                            New_Item = true
                        };
                    }
                    else
                    {
                        relClass.classItm.ClassOItem = new clsOntologyItem
                        {
                            GUID = relClass.rel.ID_Object,
                            Name = relClass.rel.Name_Object,
                            GUID_Parent = relClass.rel.ID_Parent_Object,
                            Type = globals.Type_Object,
                            New_Item = false
                        };
                    }
                });

               var classItemsToSaveAndRelate = classItems.Where(clsItm => clsItm.ClassOItem.New_Item.Value);

               var relationConfig = new clsRelationConfig(globals);
               var classItemsToSave = classItemsToSaveAndRelate.Select(clsItm => clsItm.ClassOItem).ToList();
               var classRelations = classItemsToSaveAndRelate.Select(clsItm => relationConfig.Rel_ObjectRelation(clsItm.ClassOItem, clsItm.NamespaceItem, Config.LocalData.RelationType_belongs_to)).ToList();

               if (classItemsToSave.Any())
               {
                   result.ResultState = dbConnectorClassNamespaceRelation.SaveObjects(classItemsToSave);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving Classes!";
                       return result;
                   }
               }
              
               if (classRelations.Any())
               {
                   result.ResultState = dbConnectorClassNamespaceRelation.SaveObjRel(classRelations);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving relations between classes and namespaces!";
                       return result;
                   }
               }
               
               var searchClassesToMembers = classItems.Select(cls => new clsObjectRel
               {
                   ID_Object = cls.IdClass,
                   ID_RelationType = Config.LocalData.ClassRel_Classes_contains_Member__Class_.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Classes_contains_Member__Class_.ID_Class_Right
               }).ToList();

               var dbReaderClassesToMembers = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderClassesToMembers.GetDataObjectRel(searchClassesToMembers);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Members of Classes!";
                   return result;
               }

               var searchMembersToParameters = dbReaderClassesToMembers.ObjectRels.Select(clsToMemb => new clsObjectRel
               {
                   ID_Object = clsToMemb.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_Member__Class__contains_MemberParameter.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Member__Class__contains_MemberParameter.ID_Class_Right
               }).ToList();

               var dbReaderMembersToParameters = new OntologyModDBConnector(globals);

               if (searchMembersToParameters.Any())
               {
                   result.ResultState = dbReaderMembersToParameters.GetDataObjectRel(searchMembersToParameters);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Paremters of Members!";
                       return result;
                   }
               }

               var searchParametersToTypes = dbReaderMembersToParameters.ObjectRels.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_MemberParameter_is_of_Type_DataType__Dot_Net_.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_MemberParameter_is_of_Type_DataType__Dot_Net_.ID_Class_Right
               }).ToList();

               var dbReaderParametersToTypes = new OntologyModDBConnector(globals);

               if (searchParametersToTypes.Any())
               {
                   result.ResultState = dbReaderParametersToTypes.GetDataObjectRel(searchParametersToTypes);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Datatypes of Parameters!";
                       return result;
                   }
               }

               var relationsToSave = new List<clsObjectRel>();

               foreach (var classItem in classItems)
               {
                   foreach (var member in classItem.Members)
                   {
                       var dbMemberCandidates = dbReaderClassesToMembers.ObjectRels.Where(clsToMemb => clsToMemb.ID_Object == classItem.IdClass && clsToMemb.Name_Other == member.Name).ToList();

                       if (member.Parameters.Any())
                       {
                           var dbMemberWithParams = (from dbClassToMember in dbMemberCandidates
                                                     join dbParam in dbReaderMembersToParameters.ObjectRels on dbClassToMember.ID_Other equals dbParam.ID_Object
                                                     join dbParamType in dbReaderParametersToTypes.ObjectRels on dbParam.ID_Other equals dbParamType.ID_Object
                                                     join param in member.Parameters on dbParam.Name_Other equals param.Name
                                                     where param.TypeName == dbParamType.Name_Other
                                                     select new { dbClassToMember, dbParam, param, dbParamType }).ToList();

                           if (!dbMemberWithParams.Any())
                           {
                               member.ClassMemberOItem = new clsOntologyItem
                               {
                                   GUID = globals.NewGUID,
                                   Name = member.Name,
                                   GUID_Parent = Config.LocalData.Class_Member__Class_.GUID,
                                   Type = globals.Type_Object,
                                   New_Item = true
                               };

                               relationsToSave.Add(relationConfig.Rel_ObjectRelation(classItem.ClassOItem, member.ClassMemberOItem, Config.LocalData.RelationType_contains));

                               foreach (var param in member.Parameters)
                               {
                                   param.MemberParameterOItem = new clsOntologyItem
                                   {
                                       GUID = globals.NewGUID,
                                       Name = param.Name,
                                       GUID_Parent = Config.LocalData.Class_MemberParameter.GUID,
                                       Type = globals.Type_Object,
                                       New_Item = true
                                   };

                                   param.ParamTypeOItem = paramTypes.FirstOrDefault(paramType => paramType.Name == param.TypeName);

                                   relationsToSave.Add(relationConfig.Rel_ObjectRelation(member.ClassMemberOItem, param.MemberParameterOItem, Config.LocalData.RelationType_contains));
                                   relationsToSave.Add(relationConfig.Rel_ObjectRelation(param.MemberParameterOItem, param.ParamTypeOItem, Config.LocalData.RelationType_is_of_Type));
                               }
                           }
                           else
                           {
                               var dbMember = dbMemberWithParams.First();
                               member.ClassMemberOItem = new clsOntologyItem
                               {
                                   GUID = dbMember.dbClassToMember.ID_Other,
                                   Name = dbMember.dbClassToMember.Name_Other,
                                   GUID_Parent = dbMember.dbClassToMember.ID_Parent_Other,
                                   Type = globals.Type_Object,
                                   New_Item = false
                               };

                               foreach (var memberWithParam in dbMemberWithParams)
                               {
                                   memberWithParam.param.MemberParameterOItem = new clsOntologyItem
                                   {
                                       GUID = memberWithParam.dbParam.ID_Other,
                                       Name = memberWithParam.dbParam.Name_Other,
                                       GUID_Parent = memberWithParam.dbParam.ID_Parent_Other,
                                       Type = globals.Type_Object,
                                       New_Item = false
                                   };
                                   memberWithParam.param.ParamTypeOItem = new clsOntologyItem
                                   {
                                       GUID = memberWithParam.dbParamType.ID_Other,
                                       Name = memberWithParam.dbParamType.Name_Other,
                                       GUID_Parent = memberWithParam.dbParamType.ID_Parent_Other,
                                       Type = globals.Type_Object
                                   };
                               }
                           }
                       }
                       else
                       {
                           var dbMember = (from dbClassToMember in dbMemberCandidates
                                                     join dbParam in dbReaderMembersToParameters.ObjectRels on dbClassToMember.ID_Other equals dbParam.ID_Object into dbParams
                                                     from dbParam in dbParams.DefaultIfEmpty()
                                                     where dbParam == null
                                                     select dbClassToMember).FirstOrDefault();

                           if (dbMember == null)
                           {
                               member.ClassMemberOItem = new clsOntologyItem
                               {
                                   GUID = globals.NewGUID,
                                   Name = member.Name,
                                   GUID_Parent = Config.LocalData.Class_Member__Class_.GUID,
                                   Type = globals.Type_Object,
                                   New_Item = true
                               };

                               relationsToSave.Add(relationConfig.Rel_ObjectRelation(classItem.ClassOItem, member.ClassMemberOItem, Config.LocalData.RelationType_contains));

                           }
                           else
                           {
                               member.ClassMemberOItem = new clsOntologyItem
                               {
                                   GUID = dbMember.ID_Other,
                                   Name = dbMember.Name_Other,
                                   GUID_Parent = dbMember.ID_Parent_Other,
                                   Type = globals.Type_Object,
                                   New_Item = false
                               };
                           }
                       }
                       
                   }
               }

               var membersToSave = classItems.SelectMany(cls => cls.Members).Where(memb => memb.ClassMemberOItem.New_Item.Value).ToList();
               var parametersToSave = membersToSave.SelectMany(memb => memb.Parameters).ToList();

               var objectsToSave = membersToSave.Select(memb => memb.ClassMemberOItem).ToList();
               objectsToSave.AddRange(parametersToSave.Select(param => param.MemberParameterOItem));

               var dbWriter = new OntologyModDBConnector(globals);
               if (objectsToSave.Any())
               {
                   dbWriter.SaveObjects(objectsToSave);
               }
               
               if (relationsToSave.Any())
               {
                   dbWriter.SaveObjRel(relationsToSave);
               }
               
               result.Result = classRelations;

               return result;
           });

            return taskResult;
        }
        public async Task<ResultItem<clsOntologyItem>> GetOrCreateNamespaceItem(string name, clsOntologyItem parentItem = null)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                if (parentItem == null)
                {
                    var searchNamespace = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            Name = name,
                            GUID_Parent = Config.LocalData.Class_Namespace.GUID
                        }
                    };

                    var dbConnector = new OntologyModDBConnector(globals);

                    result.ResultState = dbConnector.GetDataObjects(searchNamespace);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the namespace!";
                        return result;
                    }

                    result.Result = dbConnector.Objects1.FirstOrDefault();

                    if (result.Result == null)
                    {
                        result.Result = new clsOntologyItem
                        {
                            GUID = globals.NewGUID,
                            Name = name,
                            GUID_Parent = Config.LocalData.Class_Namespace.GUID,
                            Type = globals.Type_Object
                        };

                        result.ResultState = dbConnector.SaveObjects(new List<clsOntologyItem>
                        {
                            result.Result
                        });

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the namespace!";
                            return result;
                        }
                        
                    }
                }
                else
                {
                    var searchNamespace = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = parentItem.GUID,
                            ID_RelationType = Config.LocalData.RelationType_contains.GUID,
                            ID_Parent_Other = Config.LocalData.Class_Namespace.GUID
                        }
                    };

                    var dbConnector = new OntologyModDBConnector(globals);

                    result.ResultState = dbConnector.GetDataObjectRel(searchNamespace);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the namespace!";
                        return result;
                    }

                    result.Result = dbConnector.ObjectRels.Where(rel => rel.Name_Other == name).Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = globals.Type_Object
                    }).FirstOrDefault();

                    if (result.Result == null)
                    {
                        result.Result = new clsOntologyItem
                        {
                            GUID = globals.NewGUID,
                            Name = name,
                            GUID_Parent = Config.LocalData.Class_Namespace.GUID,
                            Type = globals.Type_Object
                        };

                        result.ResultState = dbConnector.SaveObjects(new List<clsOntologyItem>
                        {
                            result.Result
                        });

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the namespace!";
                            return result;
                        }

                        var relationConfig = new clsRelationConfig(globals);

                        var rel = relationConfig.Rel_ObjectRelation(parentItem, result.Result, Config.LocalData.RelationType_contains);

                        result.ResultState = dbConnector.SaveObjRel(new List<clsObjectRel>
                        {
                            rel
                        });

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving relation between parent-namespace and namespace!";
                            return result;
                        }
                    }
                }

                return result;
            });

            return taskResult;
        }
        public async Task<ResultItem<List<clsOntologyItem>>> SaveNewNamespaces(List<clsOntologyItem> checkItems, clsOntologyItem parentItem = null)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsOntologyItem>()
               };

               var relationConfig = new clsRelationConfig(globals);
               var dbConnector = new OntologyModDBConnector(globals);
               if (parentItem == null)
               {
                   if (checkItems.Any())
                   {
                       result.ResultState = dbConnector.GetDataObjects(checkItems);
                       if (result.ResultState.GUID == globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while getting existing Namespaces!";
                           return result;
                       }
                   }
                   

                   

                   result.Result = (from checkItem in checkItems
                                    join existItem in dbConnector.Objects1 on checkItem.Name equals existItem.Name into existItems
                                    from existItem in existItems.DefaultIfEmpty()
                                    select new { checkItem, existItem }).Select(itm =>
                                    {
                                        if (itm.existItem != null)
                                        {
                                            itm.existItem.New_Item = false;
                                            return itm.existItem;
                                        }
                                        else
                                        {
                                            itm.checkItem.GUID = globals.NewGUID;
                                            itm.checkItem.Type = globals.Type_Object;
                                            itm.checkItem.New_Item = true;
                                            return itm.checkItem;
                                        }
                                    }).ToList();

                   var saveItems = result.Result.Where(itm => itm.New_Item.Value).ToList();

                   if (saveItems.Any())
                   {
                       result.ResultState = dbConnector.SaveObjects(saveItems);

                       if (result.ResultState.GUID == globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while saving Namespaces";
                       }
                   }
                   
               }
               else
               {
                   var searchItems = new List<clsObjectRel>
                   {
                       new clsObjectRel
                       {
                           ID_Object = parentItem.GUID,
                           ID_RelationType = Config.LocalData.ClassRel_Namespace_contains_Namespace.ID_RelationType,
                           ID_Parent_Other = Config.LocalData.ClassRel_Namespace_contains_Namespace.ID_Class_Right
                       }
                   };

                   if (searchItems.Any())
                   {
                       result.ResultState = dbConnector.GetDataObjectRel(searchItems);
                       if (result.ResultState.GUID == globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while getting existing Namespaces!";
                           return result;
                       }
                   }
                   

                   result.Result = (from checkItem in checkItems
                                    join existItem in dbConnector.ObjectRels on checkItem.Name equals existItem.Name_Other into existItems
                                    from existItem in existItems.DefaultIfEmpty()
                                    select new { checkItem, existItem }).Select(itm =>
                                    {
                                        if (itm.existItem != null)
                                        {
                                            itm.checkItem.GUID = itm.existItem.ID_Other;
                                            itm.checkItem.Type = itm.existItem.Ontology;
                                            itm.checkItem.New_Item = false;
                                            
                                        }
                                        else
                                        {
                                            itm.checkItem.GUID = globals.NewGUID;
                                            itm.checkItem.Type = globals.Type_Object;
                                            itm.checkItem.New_Item = true;
                                            
                                        }
                                        return itm.checkItem;
                                    }).ToList();

                   var saveItems = result.Result.Where(itm => itm.New_Item.Value).ToList();

                   if (saveItems.Any())
                   {
                       result.ResultState = dbConnector.SaveObjects(saveItems);

                       if (result.ResultState.GUID == globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while saving Namespaces";
                       }
                   }
                   

                   var saveRels = saveItems.Select(saveItm => relationConfig.Rel_ObjectRelation(parentItem, saveItm, Config.LocalData.RelationType_contains)).ToList();

                   if (saveRels.Any())
                   {
                       result.ResultState = dbConnector.SaveObjRel(saveRels);

                       if (result.ResultState.GUID == globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while saving relation between parentitem and subitems";
                           return result;
                       }
                   }
                   
               }
               

               return result;
           });

            return taskResult;
        }
        public async Task<ResultItem<GetModelResult>> GetModel(AnalyzeAssemblyRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetModelResult>>(() =>
            {
                var result = new ResultItem<GetModelResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetModelResult()
                };

                if (string.IsNullOrEmpty(request.IdConfig) || !globals.is_GUID(request.IdConfig))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The Conifg-Id is not valid!";
                    return result;
                }

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig,
                        GUID_Parent = Config.LocalData.Class_AssemblyAnalyzeModule.GUID
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading config";
                    return result;
                }

                result.Result.RootConfig = dbReaderConfig.Objects1.FirstOrDefault();

                if (result.Result.RootConfig == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Config couldn't be found!";
                    return result;
                }

                var searchSubConfigs = dbReaderConfig.Objects1.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_AssemblyAnalyzeModule_contains_AssemblyAnalyzeModule.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_AssemblyAnalyzeModule_contains_AssemblyAnalyzeModule.ID_Class_Right
                }).ToList();

                var dbReaderSubConfigs = new OntologyModDBConnector(globals);

                if (searchSubConfigs.Any())
                {
                    result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting sub-configs";
                        return result;
                    }

                    result.Result.Configs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).ToList();
                }
                
                if (!result.Result.Configs.Any())
                {
                    result.Result.Configs.Add(result.Result.RootConfig);
                }



                var searchFileResourceOfConfig = result.Result.Configs.Select(cnfg => 
                    new clsObjectRel
                    {
                        ID_Object = cnfg.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_AssemblyAnalyzeModule_belonging_Source_Fileresource.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_AssemblyAnalyzeModule_belonging_Source_Fileresource.ID_Class_Right
                    }).ToList();

                var dbReaderFileResource = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderFileResource.GetDataObjectRel(searchFileResourceOfConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading fileresource";
                    return result;
                }

                result.Result.FileResource = dbReaderFileResource.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = globals.Type_Object
                }).FirstOrDefault();

                if (result.Result.FileResource == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Fileresource couldn't be found!";
                    return result;
                }

                if (request.GetTextParsers)
                {
                    var searchTextParser = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = request.IdConfig,
                            ID_RelationType = Config.LocalData.ClassRel_AssemblyAnalyzeModule_dst_Textparser.ID_RelationType,
                            ID_Parent_Other = Config.LocalData.ClassRel_AssemblyAnalyzeModule_dst_Textparser.ID_Class_Right
                        }
                    };

                    var dbReaderTextParser = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderTextParser.GetDataObjectRel(searchTextParser);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Textparser";
                        return result;
                    }

                    result.Result.TextParser = dbReaderTextParser.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = globals.Type_Object
                    }).FirstOrDefault();

                    if (result.Result.TextParser == null)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "Textparser couldn't be found!";
                        return result;
                    }
                }

                var searchRegexFilters = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_AssemblyAnalyzeModule_Regexfilter_Classnames_Regular_Expressions.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_AssemblyAnalyzeModule_Regexfilter_Classnames_Regular_Expressions.ID_Class_Right
                }).ToList();

                var dbReaderRegexFilters = new OntologyModDBConnector(globals);

                if (searchRegexFilters.Any())
                {
                    result.ResultState = dbReaderRegexFilters.GetDataObjectRel(searchRegexFilters);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Regex-Filters!";
                        return result;
                    }
                }

                result.Result.ConfigsToRegexFilter = dbReaderRegexFilters.ObjectRels;
                foreach (var config in result.Result.Configs)
                {
                    if (result.Result.ConfigsToRegexFilter.Where(filt => filt.Name_Object == config.Name).Count() > 1)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "You have more Regex-Filters as configs. This is not allowed!";
                        return result;
                    }
                }

                var searchRegexOfFilters = result.Result.ConfigsToRegexFilter.Select(filt => new clsObjectAtt
                {
                    ID_Object = filt.ID_Other,
                    ID_AttributeType = Config.LocalData.ClassAtt_Regular_Expressions_RegEx.ID_AttributeType
                }).ToList();

                var dbReaderRegexOffilters = new OntologyModDBConnector(globals);

                if (searchRegexOfFilters.Any())
                {
                    result.ResultState = dbReaderRegexOffilters.GetDataObjectAtt(searchRegexOfFilters);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Regex of Filters!";
                        return result;
                    }
                }

                result.Result.RegexFilterToRegex = dbReaderRegexOffilters.ObjAtts;

                if (result.Result.RegexFilterToRegex.Count != result.Result.ConfigsToRegexFilter.Count)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "You need as much Regex-Patterns as Regex-Filters!";
                    return result;
                }

                foreach (var filterToRegex in result.Result.RegexFilterToRegex)
                {
                    try
                    {
                        var regex = new Regex(filterToRegex.Val_String);
                    }
                    catch (Exception ex)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = ex.Message;
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public ElasticAgent(Globals globals)
        {
            this.globals = globals;
        }

        public clsOntologyItem InitElasticAgent()
        {
            var searchParamTypes = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = Config.LocalData.Class_DataType__Dot_Net_.GUID
                }
            };

            var dbReaderDataTypes = new OntologyModDBConnector(globals);
            var result = dbReaderDataTypes.GetDataObjects(searchParamTypes);
            paramTypes = dbReaderDataTypes.Objects1;

            return result;
        }
    }
}
