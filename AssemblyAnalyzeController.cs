﻿using AssemblyAnaylzeModule.Models;
using AssemblyAnaylzeModule.Services;
using FileResourceModule;
using LogModule;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TextParserModule;

namespace AssemblyAnaylzeModule
{
    public class AssemblyAnalyzeController : AppController
    {

        public async Task<ResultItem<clsOntologyItem>> GetNamespaceItemByFullname(string namespaceFullname)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(async () =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                clsOntologyItem parentItem = null;
                var nameSpaceSplit = namespaceFullname.Split('.');
                var elasticAgent = new ElasticAgent(Globals);

                for (int ix = 0; ix < nameSpaceSplit.Length; ix++)
                {

                    var nameSpacePart = nameSpaceSplit[ix];
                    result = await elasticAgent.GetOrCreateNamespaceItem(nameSpacePart, parentItem);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    parentItem = result.Result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<SyncClassesResult>> SyncMembers(SyncMembersRequest request)
        {
            var taskResult = await Task.Run<ResultItem<SyncClassesResult>>(async () =>
            {
                var result = new ResultItem<SyncClassesResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new SyncClassesResult()
                };

                var logController = new LogController(Globals);
                var elasticAgent = new ElasticAgent(Globals);
                elasticAgent.InitElasticAgent();
                var serviceResult = await elasticAgent.GetModel(request);

                var fileResourceConnector = new FileResourceController(Globals);

                foreach (var config in serviceResult.Result.Configs)
                {
                    var fileResourceResult = await fileResourceConnector.GetFileResource(config, Config.LocalData.RelationType_belonging_Source, Globals.Direction_LeftRight);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "FileResource cannot be build!";
                        return result;
                    }

                    if (string.IsNullOrEmpty(fileResourceResult.Result.NamePath))
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "Path is not valid!";
                        return result;

                    }
                    if (!System.IO.Directory.Exists(Environment.ExpandEnvironmentVariables(fileResourceResult.Result.NamePath)))
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "Path does not exist!";
                        return result;
                    }

                    var filesResult = await fileResourceConnector.GetFilesByFileResource(fileResourceResult.Result);

                    result.ResultState = filesResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var regexFilter = (from filter in serviceResult.Result.ConfigsToRegexFilter.Where(configToRegex => configToRegex.ID_Object == config.GUID)
                                       join regex in serviceResult.Result.RegexFilterToRegex on filter.ID_Other equals regex.ID_Object
                                       select new { regex, pattern = new Regex(regex.Val_String) }).FirstOrDefault();

                    foreach (var file in filesResult.Result)
                    {
                        var assemblyItem = new AssemblyItem
                        {
                            ResultState = Globals.LState_Success.Clone()
                        };

                        try
                        {
                            assemblyItem.Assembly = Assembly.LoadFrom(file);
                            var types = assemblyItem.Assembly.GetTypes();
                            var classes = new List<ClassItem>();
                            var members = new List<clsOntologyItem>();

                            foreach (var type in types)
                            {
                                var classItem = new ClassItem
                                {
                                    NameClass = type.Name
                                };
                                if (string.IsNullOrEmpty(type.Namespace))
                                {
                                    continue;
                                }

                                if (regexFilter != null)
                                {
                                    if (!regexFilter.pattern.Match(type.Name).Success)
                                    {
                                        continue;
                                    }
                                }

                                var nameSpace = type.Namespace;
                                var nameSpaceSplit = nameSpace.Split('.');
                                clsOntologyItem parentItem = null;
                                for (int ix = 0; ix < nameSpaceSplit.Length; ix++)
                                {

                                    var nameSpacePart = nameSpaceSplit[ix];

                                    var serviceResultNameSpace = await elasticAgent.GetOrCreateNamespaceItem(nameSpacePart, parentItem);
                                    result.ResultState = serviceResultNameSpace.ResultState;

                                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                    {
                                        return result;
                                    }

                                    parentItem = serviceResultNameSpace.Result;
                                    
                                }

                                if (parentItem == null)
                                {
                                    result.ResultState = Globals.LState_Error.Clone();
                                    result.ResultState.Additional1 = "No Namespace Item can be found!";
                                    return result;
                                }

                                classItem.NamespaceItem = parentItem;

                                classes.Add(classItem);

                                var methods = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                                classItem.Members.AddRange(methods.Select(method => new ClassMember
                                {
                                    Name = method.Name,
                                    Parameters = method.GetParameters().Select(param => new MemberParameter
                                    {
                                        Name = param.Name,
                                        TypeName = param.ParameterType.FullName
                                    }).ToList()
                                }));

                                if (classes.Count > 100)
                                {
                                    var serviceResultCheck = await elasticAgent.CheckClasses(classes);
                                    result.ResultState = serviceResultCheck.ResultState;
                                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                    {
                                        return result;
                                    }
                                    result.Result.FoundClasses += classes.Count;
                                    result.Result.SavedClasses += serviceResultCheck.Result.Count;

                                    classes.Clear();
                                }


                                
                            }

                            if (classes.Any())
                            {
                                var serviceResultCheck = await elasticAgent.CheckClasses(classes);
                                result.ResultState = serviceResultCheck.ResultState;
                                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                {
                                    return result;
                                }
                                result.Result.FoundClasses += classes.Count;
                                result.Result.SavedClasses += serviceResultCheck.Result.Count;

                                classes.Clear();
                            }

                            

                            
                        }
                        catch (Exception ex)
                        {
                            assemblyItem.ResultState = Globals.LState_Error.Clone();
                            assemblyItem.ResultState.Additional1 = ex.Message;


                        }
                         
                    }

                }

                return result;
            });

            return taskResult;


        }

        public async Task<OntoMsg_Module.Models.ResultItem<List<NameSpaceItem>>> SyncNameSpaces(SyncNamespacesRequest request)
        {
            var taskResult = await Task.Run<OntoMsg_Module.Models.ResultItem<List<NameSpaceItem>>>(async () =>
           {
               var result = new OntoMsg_Module.Models.ResultItem<List<NameSpaceItem>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<NameSpaceItem>()
               };

               var logController = new LogController(Globals);
               var elasticAgent = new ElasticAgent(Globals);
               var serviceResult = await elasticAgent.GetModel(request);

               result.ResultState = serviceResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               //var logEntryResult = await logController.CreateLogEntry(LogType.Success, serviceResult.Result.RootConfig, request.UserItem);

               //result.ResultState = logEntryResult.ResultState;

               //if (result.ResultState.GUID == Globals.LState_Error.GUID)
               //{
               //    result.ResultState.Additional1 = "Error while creating a Logentry";
               //    return result;
               //}

               var fileResourceConnector = new FileResourceController(Globals);



               var nameSpaces = new List<string>();

               foreach (var config in serviceResult.Result.Configs)
               {
                   var fileResourceResult = await fileResourceConnector.GetFileResource(config, Config.LocalData.RelationType_belonging_Source, Globals.Direction_LeftRight);

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "FileResource cannot be build!";
                       return result;
                   }

                   if (string.IsNullOrEmpty(fileResourceResult.Result.NamePath))
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "Path is not valid!";
                       return result;

                   }
                   if (!System.IO.Directory.Exists(Environment.ExpandEnvironmentVariables(fileResourceResult.Result.NamePath)))
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "Path does not exist!";
                       return result;
                   }

                   var filesResult = await fileResourceConnector.GetFilesByFileResource(fileResourceResult.Result);

                   result.ResultState = filesResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       return result;
                   }
                   foreach (var file in filesResult.Result)
                   {
                       if (request.CancellationToken.IsCancellationRequested)
                       {
                           break;
                       }
                       var assemblyItem = new AssemblyItem
                       {
                           ResultState = Globals.LState_Success.Clone()
                       };

                       try
                       {
                           assemblyItem.Assembly = Assembly.LoadFrom(file);
                           var types = assemblyItem.Assembly.GetTypes();
                           foreach (var type in types)
                           {
                               var nameSpace = type.Namespace;

                               if (!nameSpaces.Contains(nameSpace))
                               {
                                   nameSpaces.Add(nameSpace);
                               }
                           }
                       }
                       catch (Exception ex)
                       {
                           assemblyItem.ResultState = Globals.LState_Error.Clone();
                           assemblyItem.ResultState.Additional1 = ex.Message;


                       }


                   }

                   var nameSpaceItems = new List<NameSpaceItem>();

                   foreach (var nameSpace in nameSpaces.Where(nameSp => !string.IsNullOrEmpty(nameSp)))
                   {
                       var nameSpaceSplit = nameSpace.Split('.');
                       NameSpaceItem parentNameSpaceItem = null;
                       for (int ix = 0; ix < nameSpaceSplit.Length; ix++)
                       {

                           var nameSpacePart = nameSpaceSplit[ix];
                           var fullPath = "";
                           for (int jx = 0; jx <= ix; jx++)
                           {
                               if (!string.IsNullOrEmpty(fullPath))
                               {
                                   fullPath += ".";
                               }
                               fullPath += nameSpaceSplit[jx];
                           }

                           if (ix == 0)
                           {
                               parentNameSpaceItem = nameSpaceItems.FirstOrDefault(nameSp => nameSp.FullPath == fullPath);
                           }

                           var nameSpaceItem = nameSpaceItems.Select(nameSp => nameSp.Find(fullPath)).Where(nameSp => nameSp != null).FirstOrDefault();

                           if (nameSpaceItem == null)
                           {
                               nameSpaceItem = new NameSpaceItem
                               {
                                   Name = nameSpacePart,
                                   FullPath = fullPath
                               };

                               if (parentNameSpaceItem != null)
                               {
                                   nameSpaceItem.ParentItem = parentNameSpaceItem;
                                   parentNameSpaceItem.SubItems.Add(nameSpaceItem);
                               }
                               else
                               {

                                   nameSpaceItems.Add(nameSpaceItem);
                               }



                           }
                           parentNameSpaceItem = nameSpaceItem;

                       }
                   }


                   var parentItems = nameSpaceItems.Where(nameSp => nameSp.Name.Length > 1 && !nameSp.Name.Any(nameChar => char.IsControl(nameChar))).Select(nameSp => new clsOntologyItem
                   {
                       Name = nameSp.Name,
                       GUID_Parent = Config.LocalData.Class_Namespace.GUID
                   }).ToList();

                   var saveResult = await elasticAgent.SaveNewNamespaces(parentItems);

                   result.ResultState = saveResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       return result;
                   }

               (from parentItem in nameSpaceItems
                join objectItem in saveResult.Result on parentItem.Name equals objectItem.Name
                select new { parentItem, objectItem }).ToList().ForEach(itm =>
                {
                    itm.parentItem.NameSpaceOItem = itm.objectItem;
                });

                   foreach (var namespaceItem in nameSpaceItems)
                   {
                       result.ResultState = await SyncNameSpaces(namespaceItem, elasticAgent);
                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           return result;
                       }
                   }

                   result.Result.AddRange(nameSpaceItems);

               }



               return result;
           });

            return taskResult;

        }

        private async Task<clsOntologyItem> SyncNameSpaces(NameSpaceItem parentItem, ElasticAgent serviceAgent)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {

                var result = Globals.LState_Success.Clone();
                var items = parentItem.SubItems.Select(nameSp => new clsOntologyItem
                {
                    Name = nameSp.Name,
                    GUID_Parent = Config.LocalData.Class_Namespace.GUID
                }).ToList();

                var resultSave = await serviceAgent.SaveNewNamespaces(items, parentItem.NameSpaceOItem);

                result = resultSave.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                (from subItem in parentItem.SubItems
                 join oItem in resultSave.Result on subItem.Name equals oItem.Name
                 select new { subItem, oItem }).ToList().ForEach(itm =>
                 {
                     itm.subItem.NameSpaceOItem = itm.oItem;
                 });

                foreach (var subItem in parentItem.SubItems)
                {
                    result = await SyncNameSpaces(subItem, serviceAgent);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                return result;
            });

            return taskResult;



        }

        public async Task<OntoMsg_Module.Models.ResultItem<AnalyzeAssemblyResult>> AnalyzeAssemblyIntoESIndex(AnalyzeAssemblyRequest request)
        {
            var result = new OntoMsg_Module.Models.ResultItem<AnalyzeAssemblyResult>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new AnalyzeAssemblyResult
                {
                    ESDocCount = 0,
                    Start = DateTime.Now,
                    End = DateTime.Now
                }
            };

            request.GetTextParsers = true;
            var elasticAgent = new ElasticAgent(Globals);
            var serviceResult = await elasticAgent.GetModel(request);

            result.ResultState = serviceResult.ResultState;

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var fileResourceConnector = new FileResourceController(Globals);

            var fileResourceResult = await fileResourceConnector.GetFileResource(serviceResult.Result.RootConfig, Config.LocalData.RelationType_belonging_Source, Globals.Direction_LeftRight);

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "FileResource cannot be build!";
                return result;
            }

            if (string.IsNullOrEmpty(fileResourceResult.Result.NamePath))
            {
                result.ResultState = Globals.LState_Error.Clone();
                result.ResultState.Additional1 = "Path is not valid!";
                return result;

            }
            if (!System.IO.Directory.Exists(Environment.ExpandEnvironmentVariables(fileResourceResult.Result.NamePath)))
            {
                result.ResultState = Globals.LState_Error.Clone();
                result.ResultState.Additional1 = "Path does not exist!";
                return result;
            }

            var textParserConnector = new TextParserController(Globals);

            var textParserResult = await textParserConnector.GetTextParser(serviceResult.Result.TextParser);

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while getting the Textparser!";
                return result;
            }

            var textParser = textParserResult.Result.FirstOrDefault();

            if (textParser == null)
            {
                result.ResultState = Globals.LState_Error.Clone();
                result.ResultState.Additional1 = "Textparser not found!";
                return result;
            }

            if (string.IsNullOrEmpty(textParser.NameIndexElasticSearch) ||
                string.IsNullOrEmpty(textParser.NameServer) ||
                textParser.Port == 0 ||
                string.IsNullOrEmpty(textParser.NameEsType))
            {
                result.ResultState = Globals.LState_Error.Clone();
                result.ResultState.Additional1 = "Textparser-Configuration is not valid!";
                return result;
            }

            var filesResult = await fileResourceConnector.GetFilesByFileResource(fileResourceResult.Result);

            result.ResultState = filesResult.ResultState;

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var docs = new List<clsAppDocuments>();




            var dbReader = new ElasticSearchNestConnector.clsUserAppDBSelector(textParser.NameServer, textParser.Port, textParser.NameIndexElasticSearch, Globals.SearchRange, Globals.Session);
            result.ResultState = dbReader.DeleteIndex(textParser.NameIndexElasticSearch);
            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var dbWriter = new ElasticSearchNestConnector.clsUserAppDBUpdater(dbReader);

            result.Result.AssemblyCountFound = filesResult.Result.Count;
            result.Result.AssemblyCountDone = 0;
            foreach (var file in filesResult.Result)
            {
                if (request.CancellationToken.IsCancellationRequested)
                {
                    break;
                }
                var assemblyItem = new AssemblyItem
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                try
                {
                    assemblyItem.Assembly = Assembly.LoadFrom(file);
                    var types = assemblyItem.Assembly.GetTypes();
                    foreach (var type in types)
                    {
                        if (request.CancellationToken.IsCancellationRequested)
                        {
                            break;
                        }
                        var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Static);
                        var methods = type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

                        foreach (var property in properties)
                        {
                            if (request.CancellationToken.IsCancellationRequested)
                            {
                                break;
                            }
                            var doc = new clsAppDocuments
                            {
                                Id = Globals.NewGUID,
                                Dict = new Dictionary<string, object>()
                            };

                            docs.Add(doc);

                            var assemblyName = assemblyItem.Assembly.GetName();
                            doc.Dict.Add("Assembly", assemblyName.Name);
                            doc.Dict.Add("TypeName", type.Name);
                            doc.Dict.Add("version", $"{assemblyName.Version.Major}.{assemblyName.Version.Minor}.{assemblyName.Version.Revision}.{assemblyName.Version.MajorRevision}.{assemblyName.Version.MinorRevision}");
                            doc.Dict.Add("Path", file);
                            doc.Dict.Add("IsProperty", true);
                            doc.Dict.Add("IsPublic", property.PropertyType.IsPublic);
                            doc.Dict.Add("Membername", property.Name);
                            doc.Dict.Add("Membertype", property.PropertyType.ToString());
                            result.Result.ESDocCount++;

                            if (docs.Count > 5000)
                            {
                                result.ResultState = dbWriter.SaveDoc(docs, textParser.NameEsType);
                                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                {
                                    result.ResultState.Additional1 = "Error while saveing of documents!";
                                    return result;
                                }
                                docs.Clear();
                            }

                        }

                        foreach (var method in methods)
                        {
                            if (request.CancellationToken.IsCancellationRequested)
                            {
                                break;
                            }
                            var doc = new clsAppDocuments
                            {
                                Id = Globals.NewGUID,
                                Dict = new Dictionary<string, object>()
                            };

                            docs.Add(doc);

                            var assemblyName = assemblyItem.Assembly.GetName();
                            doc.Dict.Add("Assembly", assemblyName.Name);
                            doc.Dict.Add("TypeName", type.Name);
                            doc.Dict.Add("Version", $"{assemblyName.Version.Major}.{assemblyName.Version.Minor}.{assemblyName.Version.Revision}.{assemblyName.Version.MajorRevision}.{assemblyName.Version.MinorRevision}");
                            doc.Dict.Add("Path", file);
                            doc.Dict.Add("IsProperty", false);
                            doc.Dict.Add("IsPublic", method.IsPublic);
                            doc.Dict.Add("IsStatic", method.IsStatic);
                            doc.Dict.Add("Membername", method.Name);
                            doc.Dict.Add("Membertype", method.ReturnType.ToString());
                            var arguments = string.Join(", ", method.GetParameters().Select(arg => $"{arg.ParameterType.ToString()} {arg.Name}"));

                            doc.Dict.Add("Arguments", arguments);
                            result.Result.ESDocCount++;
                            if (docs.Count > 5000)
                            {
                                result.ResultState = dbWriter.SaveDoc(docs, textParser.NameEsType);
                                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                {
                                    result.ResultState.Additional1 = "Error while saveing of documents!";
                                    return result;
                                }
                                docs.Clear();
                            }
                        }
                    }

                    if (docs.Count > 0)
                    {
                        result.ResultState = dbWriter.SaveDoc(docs, textParser.NameEsType);
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saveing of documents!";
                            return result;
                        }
                        docs.Clear();
                    }
                }
                catch (Exception ex)
                {
                    assemblyItem.ResultState = Globals.LState_Error.Clone();
                    assemblyItem.ResultState.Additional1 = ex.Message;


                }

                result.Result.AssemblyCountDone++;
            }

            result.Result.End = DateTime.Now;
            return result;
        }

        public async Task<ResultItem<GetServiceReferencesHierarchyResult>> GetServiceReferences(GetServiceReferencesHierarchyRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetServiceReferencesHierarchyResult>>(() =>
           {
               var result = new ResultItem<GetServiceReferencesHierarchyResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new GetServiceReferencesHierarchyResult()
               };

               request.MessageOutput?.OutputInfo("Validate request...");
               result.ResultState = Validation.ValidationController.ValidateGetServiceReferencesHierarchyRequest(request, Globals);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Validated request.");

               request.MessageOutput?.OutputInfo("Get project-files...");

               var regexReferenceRaw = new Regex(@".*Service References\\.*");
               var regexReplaceBefore = new Regex(@".*Service References\\");
               var regexReplaceAfter = new Regex(@"\\.*");
               var regexReference = new Regex("[A-Za-z0-9]+");
               var regexUrlLine = new Regex("<MetadataSource Address=\".*\" Protocol=\".*\" SourceId=\".*\" />");
               var regexReplaceUrlBefore = new Regex("<MetadataSource Address=\"");
               var regexReplaceUrlAfter = new Regex("\" Protocol=\".*\" SourceId=\".*\" />");

               try
               {
                   var projectFiles = System.IO.Directory.GetFiles(request.RootPathProjects, "*.csproj", System.IO.SearchOption.AllDirectories).ToList();
                   request.MessageOutput?.OutputInfo($"Have {projectFiles.Count} files.");

                   request.MessageOutput?.OutputInfo("Analyze files...");

                   for (int i = 0; i < projectFiles.Count; i++)
                   {
                       
                       if (request.CancellationToken.IsCancellationRequested)
                       {
                           result.ResultState = Globals.LState_Nothing.Clone();
                           result.ResultState.Additional1 = "Cancelled by user.";
                           request.MessageOutput?.OutputWarning(result.ResultState.Additional1);
                           return result;
                       }

                       var fileName = projectFiles[i];

                       var content = System.IO.File.ReadAllText(fileName);

                       var referenceLineMatches = regexReferenceRaw.Matches(content);

                       foreach (Match referenceLineMatch in referenceLineMatches)
                       {
                           
                           var line = referenceLineMatch.Value;
                           var serviceReference = regexReplaceBefore.Replace(line, "");
                           serviceReference = regexReplaceAfter.Replace(serviceReference, "");
                           var serviceReferenceMatch = regexReference.Match(serviceReference);

                           if (serviceReferenceMatch.Success)
                           {
                               var referenceItem = new ResultItem<ServiceReferenceItem>
                               {
                                   ResultState = Globals.LState_Success.Clone()
                               };
                               
                               referenceItem.Result = new ServiceReferenceItem(Globals.NewGUID, serviceReferenceMatch.Value, fileName);

                               if (!result.Result.ServiceReferenceResultItems.Any(reference => reference.Result.ProjectFilePath == reference.Result.ProjectFilePath && 
                                    reference.Result.Name == referenceItem.Result.Name))
                               {
                                   result.Result.ServiceReferenceResultItems.Add(referenceItem);
                                   serviceReference = serviceReferenceMatch.Value;
                                   var pathReferenceMap = Path.GetDirectoryName(fileName);
                                   pathReferenceMap = Path.Combine(pathReferenceMap, "Service References");
                                   pathReferenceMap = Path.Combine(pathReferenceMap, serviceReference);
                                   pathReferenceMap = Path.Combine(pathReferenceMap, "Reference.svcmap");

                                   if (File.Exists(pathReferenceMap))
                                   {
                                       var contentReferenceMap = File.ReadAllText(pathReferenceMap);
                                       var urlMatch = regexUrlLine.Match(contentReferenceMap);
                                       if (urlMatch.Success)
                                       {
                                           var url = urlMatch.Value;
                                           url = regexReplaceUrlBefore.Replace(url, "");
                                           url = regexReplaceUrlAfter.Replace(url, "");

                                           if (!string.IsNullOrEmpty(url))
                                           {
                                               referenceItem.Result.Url = url;
                                           }

                                       }
                                       else
                                       {
                                           referenceItem.ResultState = Globals.LState_Error.Clone();
                                           referenceItem.ResultState.Additional1 = "Url cannot be found!";
                                       }
                                   }
                                   else
                                   {
                                       referenceItem.ResultState = Globals.LState_Error.Clone();
                                       referenceItem.ResultState.Additional1 = "Url cannot be found!";
                                   }
                               }
                               

                           }
                           
                           

                       }
                       

                       if (i > 0 && (i % 20 == 0))
                       {
                           request.MessageOutput?.OutputInfo($"Analyzed {i + 1} files.");
                       }
                   }

                   request.MessageOutput?.OutputInfo($"Analyzed {projectFiles.Count} files.");
               }
               catch (Exception ex)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = ex.Message;
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
                   
               }
               

               

               return result;
           });

            return taskResult;
        }

        public AssemblyAnalyzeController(Globals globals) : base(globals)
        {
        }
    }
}
