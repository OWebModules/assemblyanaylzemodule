﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAnaylzeModule.Models
{
    public class SyncClassesResult
    {
        public long FoundClasses { get; set; }
        public long SavedClasses { get; set; }
    }
}
