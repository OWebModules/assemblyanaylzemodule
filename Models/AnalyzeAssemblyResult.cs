﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TextParserModule.Models;

namespace AssemblyAnaylzeModule.Models
{
    public class AnalyzeAssemblyResult
    {
        public TextParser TextParser { get; set; }
        

        public long AssemblyCountFound { get; set; }
        public long AssemblyCountDone { get; set; }
        public long ESDocCount { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public TimeSpan Duration
        {
            get
            {
                return End.Subtract(Start);
            }
        }
    }

    public class AssemblyItem
    {
        public clsOntologyItem ResultState { get; set; }
        public Assembly Assembly { get; set; }
    }
}
