﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AssemblyAnaylzeModule.Models
{
    public class AnalyzeAssemblyRequest
    {
        public string IdConfig { get; private set; }
        public CancellationToken CancellationToken { get; set; }
        public clsOntologyItem UserItem { get; set; }

        public bool GetTextParsers { get; set; }

        public AnalyzeAssemblyRequest(string idConfig, CancellationToken cancellationToken, clsOntologyItem userItem)
        {
            IdConfig = idConfig;
            CancellationToken = cancellationToken;
            UserItem = userItem;
        }
    }
}
