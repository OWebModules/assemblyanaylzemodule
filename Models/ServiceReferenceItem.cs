﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAnaylzeModule.Models
{
    public class ServiceReferenceItem
    {
        public string ProjectFilePath { get; private set; }
        public string Id { get; private set; }
        public string Name { get; private set; }
        public string Url { get; set; }

        public List<ServiceReferenceItem> SubItems { get; set; } = new List<ServiceReferenceItem>();

        public ServiceReferenceItem(string id, string name, string projectFilePath)
        {
            Id = id;
            Name = name;
            ProjectFilePath = projectFilePath;
        }
    }
}
