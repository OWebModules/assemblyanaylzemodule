﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAnaylzeModule.Models
{
    public class NameSpaceItem
    {
        public string Name { get; set; }

        public string FullPath { get; set; }

        public clsOntologyItem NameSpaceOItem { get; set; }

        public NameSpaceItem ParentItem { get; set; }
        public List<NameSpaceItem> SubItems { get; set; } = new List<NameSpaceItem>();

        public NameSpaceItem Find(string fullPath)
        {
            if (FullPath == fullPath)
            {
                return this;
            }
            return SubItems.Select(subItm => subItm.Find(fullPath)).Where(subItm => subItm != null).FirstOrDefault();
        }

    }
}
