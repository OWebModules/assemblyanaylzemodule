﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAnaylzeModule.Models
{
    public class GetModelResult
    {
        public clsOntologyItem RootConfig { get; set; }

        public List<clsOntologyItem> Configs { get; set; }
        public clsOntologyItem FileResource { get; set; }
        public clsOntologyItem TextParser { get; set; }

        public List<clsObjectRel> ConfigsToRegexFilter { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> RegexFilterToRegex { get; set; } = new List<clsObjectAtt>();


    }
}
