﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAnaylzeModule.Models
{
    public class GetServiceReferencesHierarchyResult
    {
        public List<ResultItem<ServiceReferenceItem>> ServiceReferenceResultItems { get; set; } = new List<ResultItem<ServiceReferenceItem>>();

    }

}
