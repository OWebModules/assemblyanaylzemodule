﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAnaylzeModule.Models
{
    public class ClassMember
    {
        public string Id 
        {
            get { return ClassMemberOItem?.GUID; }
        }
        public string Name { get; set; }

        public clsOntologyItem ClassMemberOItem { get; set; }

        public List<MemberParameter> Parameters { get; set; } = new List<MemberParameter>();
    }
}
