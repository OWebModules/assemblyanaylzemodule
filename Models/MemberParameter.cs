﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAnaylzeModule.Models
{
    public class MemberParameter
    {
        public string Id 
        { 
            get
            {
                return MemberParameterOItem?.GUID;
            }
        }
        public string Name { get; set; }
        public string TypeName { get; set; }

        public clsOntologyItem MemberParameterOItem { get; set; }
        public clsOntologyItem ParamTypeOItem { get; set; }
    }
}
