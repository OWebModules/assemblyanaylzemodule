﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AssemblyAnaylzeModule.Models
{
    public class SyncMembersRequest : AnalyzeAssemblyRequest
    {
        public SyncMembersRequest(string idConfig, CancellationToken cancellationToken, clsOntologyItem userItem) : base(idConfig, cancellationToken, userItem)
        {
            
        }
    }
}
