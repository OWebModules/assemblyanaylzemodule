﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AssemblyAnaylzeModule.Models
{
    public class GetServiceReferencesHierarchyRequest
    {
        public string RootPathProjects { get; private set; }
        public CancellationToken CancellationToken { get; private set; }  
        public IMessageOutput MessageOutput { get; set; }

        public GetServiceReferencesHierarchyRequest(string rootPathProjects, CancellationToken cancellationToken)
        {
            RootPathProjects = rootPathProjects;
            CancellationToken = cancellationToken;
        }
    }
}
