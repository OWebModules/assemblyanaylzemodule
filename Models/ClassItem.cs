﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAnaylzeModule.Models
{
    public class ClassItem
    {
        public clsOntologyItem NamespaceItem { get; set; }
        public string IdClass 
        { 
            get
            {
                return ClassOItem?.GUID;
            }
        }
        public string NameClass { get; set; }

        public clsOntologyItem ClassOItem { get; set; }

        public List<ClassMember> Members { get; set; } = new List<ClassMember>();


    }
}
