﻿using AssemblyAnaylzeModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAnaylzeModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateGetServiceReferencesHierarchyRequest(GetServiceReferencesHierarchyRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(request.RootPathProjects))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Rootpath is empty!";
                return result;
            }

            try
            {
                if (!Directory.Exists(request.RootPathProjects))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The rootpath is not existing!";
                    return result;
                }
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }

            return result;
        }
    }
}
